# coding=utf-8

from django.shortcuts import render
from django.http import HttpResponse

# Groups views


def groups_list(request):
    groups = (
        {
            'id': 1,
            'name': u'MтМ-21',
            'leader':
                {
                    'id': 1,
                    'first_name': u'Андрей',
                    'last_name': u'Корост'
                }
        },

        {
            'id': 2,
            'name': u'MтМ-22',
            'leader':
                {
                    'id': 2,
                    'first_name': u'Игорь',
                    'last_name': u'Петров'
                }
        },
        {
            'id': 3,
            'name': u'MтМ-23',
            'leader':
                {
                    'id': 3,
                    'first_name': u'Петр',
                    'last_name': u'Иванов'
                }
        }
    )
    context = {'groups': groups}
    return render(request, 'students/group_list.html', context)


def groups_add(request):
    return HttpResponse('<h1>Group Add Form</h1>')


def groups_edit(request, gid):
    return HttpResponse('<h1>Edit Group %s</h1>' % gid)


def groups_delete(request, gid):
    return HttpResponse('<h1>Delete Group %s</h1>' % gid)