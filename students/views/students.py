# coding=utf-8

from django.shortcuts import render
from django.http import HttpResponse


# Student views


def students_list(request):
    students = (
        {
            'id': 1,
            'first_name': u'Андрей',
            'last_name': u'Корост',
            'ticket': 235,
            'image': 'img/student.jpeg'},
        {
            'id': 2,
            'first_name': u'Игорь',
            'last_name': u'Петров',
            'ticket': 1235,
            'image': 'img/student.jpeg'},
        {
            'id': 3,
            'first_name': u'Петр',
            'last_name': u'Иванов',
            'ticket': 1005,
            'image': 'img/student.jpeg'},
    )
    context = {'students': students}
    return render(request, 'students/students_list.html', context)


def students_add(request):
    return HttpResponse('<h1>Student Add Form</h1>')


def students_edit(request, sid):
    return render(request, 'students/student_edit.html', {})


def students_delete(request, sid):
    return HttpResponse('<h1>Delete Student %s</h1>' % sid)
