# coding=utf-8

from django.shortcuts import render
# from django.http import HttpResponse


# Journal views


def journals_list(request):
    journal = (
        {
            'id': 1,
            'first_name': u'Андрей',
            'last_name': u'Корост'
        },
        {
            'id': 2,
            'first_name': u'Игорь',
            'last_name': u'Петров'
        },
        {
            'id': 3,
            'first_name': u'Петр',
            'last_name': u'Иванов'
        }
    )
    context = {'journal': journal}
    return render(request, 'students/journals.html', context)
