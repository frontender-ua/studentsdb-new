# coding=utf-8
from .settings import PORTAL_URL


def students_proc(request):
    groups = (
        {
            'id': 1,
            'name': u'MтМ-21',
            'leader':
                {
                    'id': 1,
                    'first_name': u'Андрей',
                    'last_name': u'Корост'
                }
        },

        {
            'id': 2,
            'name': u'MтМ-22',
            'leader':
                {
                    'id': 2,
                    'first_name': u'Игорь',
                    'last_name': u'Петров'
                }
        },
        {
            'id': 3,
            'name': u'MтМ-23',
            'leader':
                {
                    'id': 3,
                    'first_name': u'Петр',
                    'last_name': u'Иванов'
                }
        }
    )
    return {'PORTAL_URL': PORTAL_URL, 'groups_list': groups}
